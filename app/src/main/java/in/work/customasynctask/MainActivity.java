package in.work.customasynctask;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new MyBackgroundTask().execute(1);
    }

    public class MyBackgroundTask extends CustomAsyncTask<Integer,Void,Long> {

        @Override
        public void onPreExecute() {
            Toast.makeText(MainActivity.this,"Pre execute",Toast.LENGTH_SHORT).show();
            Log.d(getClass().getSimpleName(),"pre execute");
        }

        @Override
        public void onPostExecute(Long result) {
            Toast.makeText(MainActivity.this,"Post execute",Toast.LENGTH_SHORT).show();
        }

        @Override
        public Long doInBackground(Integer... integers) {
            long sum = 0;
            long rand = (long) (Math.random() * 100);

            for(long i = 0; i <= rand; i++) {
                sum += i;
            }
            return sum;
        }
    }
}

