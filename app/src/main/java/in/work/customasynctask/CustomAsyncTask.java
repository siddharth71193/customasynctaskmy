package in.work.customasynctask;

import android.os.Handler;
import android.os.Looper;


public abstract class CustomAsyncTask<Params,Progress,Result> {

    Result result;
    Params[] params;

    public abstract void onPreExecute();

    public abstract void onPostExecute(Result result);

    public abstract Result doInBackground(Params... params);

    public void onProgressUpdate(Progress... progress){}

    public CustomAsyncTask(){
    }

    public void execute(final Params... params){
        this.params = params;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                onPreExecute();

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        result = doInBackground(CustomAsyncTask.this.params);

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {

                                onPostExecute(result);
                            }
                        });

                    }
                }).start();

            }
        });
    }
}
